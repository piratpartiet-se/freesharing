# Freesharing.se

## Vår vision
Att riva de hinder upphovsrätten ställer upp för det digitala kulturlivet.

## Vad vill vi göra?
Vårt mål är att legalisera delning – via digitala nät för personligt bruk utan vinstsyfte – av upphovsrättsskyddat material. Genom att göra det vill vi hitta en bättre balans mellan upphovsmäns och andra rättsinnehavares rättigheter och den allmänna rätten till vetenskap och kultur. I dagens digitala samhälle är vardagligt deltagande i kulturlivet ofta illegalt. Vi vill legalisera sådant som let's plays, react-videor, memes och annat skapande som bygger på existerande verk.

## Varför samlar vi in namn?
Freedom to Share är ett europeiskt medborgarinitiativ. Lyckas vi samla tillräckligt många namnunderskrifter kommer vi kunna lyfta frågan till debatt i EU-parlamentet och tvinga EU-kommissionen att ta formell ställning i frågan.

## Licens
Freedom to Share är ett europeiskt medborgarinitiativ.
Allt material på den här sidan är fritt att kopiera, remixa och sprida under licens [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.sv).